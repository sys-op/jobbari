# This Dockerfile is used to build an image containing basic stuff to be used as a Jenkins job runner for Java applications
FROM openjdk:8-jdk
MAINTAINER Alexander Muravya (aka kyberorg) <kyberorg@yadev.ee>

# In case you need proxy for apt
#RUN echo 'Acquire::http::Proxy "http://127.0.0.1:8080";' >> /etc/apt/apt.conf

## Tools ##

# Maven
RUN cd /tmp && wget http://apache.mirrors.spacedump.net/maven/maven-3/3.6.0/binaries/apache-maven-3.6.0-bin.tar.gz && tar xzfv apache-maven-3.6.0-bin.tar.gz && mv apache-maven-3.6.0 /opt/mvn && ln -s /opt/mvn/bin/mvn /usr/bin/mvn

# Git
RUN apt-get update && apt-get -y install git

# Docker
RUN curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh
COPY docker /etc/init.d/docker
RUN chmod +x /etc/init.d/docker && update-rc.d docker enable

## Actions ##

# Root it
USER root

# Run it
ENTRYPOINT exec ping localhost

